# Ordonnancement de tâches

Ce répertoire contient des fichiers permettant de résoudre à l'aide de la
recherche locale un problème d'ordonnancement de tâches.

Plus précisément, étant donnés

* un ensemble de tâches $`J`$ et
* un ensemble de machines $`M`$
* une fonction $`t : J \rightarrow \mathbb{R}_+`$ indiquant, pour chaque tâche,
  le temps pris pour compléter son traitement

on cherche une fonction $`S : J \rightarrow M`$ qui assigne à chaque tâche une
machine de telle sorte que le temps maximum de traitement par machine soit
minimal.

## Fonctionnement

Il y a un `Makefile` inclus dans le répertoire. Les cibles suivantes sont disponibles:

* `make` permet de compiler le code source avec GCC
* `make data` permet de générer des ensembles de tâches aléatoires pour tester
  le programme
* `make bench` permet de comparer les valeurs obtenues par la solution naïve et
  par la solution approchée lorsqu'on les lance sur le jeu de données généré
  par `make data`
* `make clean` permet de nettoyer les fichiers générés

## Exemple

En entrant successivement les commandes

```sh
make
make data
make bench > examples/times.csv
make plot
```

on devrait obtenir un graphique semblable à celui-ci:

![Temps de calcul selon le nombre de sommets](examples/times.png)

## Dépendances

* [GCC](https://gcc.gnu.org/)
* [Python](https://www.python.org/)
* [Gnuplot](https://gnuplot.org)

## À faire

* Rendre le script Gnuplot plus générique pour qu'il lise n'importe quel
  fichier
