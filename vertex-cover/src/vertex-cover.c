#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

/*********
 * Graph *
 *********/

struct Graph {                 // A simple graph
    unsigned int num_vertices; // Its number of vertices
    unsigned int num_edges;    // Its number of edges
    bool **edges;              // edges[u][v] iff {u,v} is an edge
};

/**
 * Loads a graph from `stdin`.
 *
 * Note: the function is unsafe, i.e. it assumes the graph format is valid
 *
 * @param graph  The graph to load to
 */
void load_graph(struct Graph *graph) {
    scanf("%d %d\n", &graph->num_vertices, &graph->num_edges);
    graph->edges = malloc(graph->num_vertices * sizeof(bool*));
    for (int v = 0; v < graph->num_vertices; ++v) {
        graph->edges[v] = calloc(graph->num_vertices, sizeof(bool));
    }
    for (int e = 0; e < graph->num_edges; ++e) {
        unsigned int u, v;
        scanf("%d %d\n", &u, &v);
        graph->edges[u][v] = true;
        graph->edges[v][u] = true;
    }
}

/**
 * Prints a graph on `stdout`.
 *
 * @param graph  The graph to print
 */
void print_graph(const struct Graph *graph) {
    printf("Graph of %d vert%s and %d edge%s\n",
           graph->num_vertices,
           graph->num_vertices > 1 ? "ices" : "ex",
           graph->num_edges,
           graph->num_edges > 1 ? "s" : "");
    for (unsigned int u = 0; u < graph->num_vertices; ++u) {
        for (unsigned int v = u + 1; v < graph->num_vertices; ++v) {
            if (graph->edges[u][v]) printf("  %d -- %d\n", u, v);
        }
    }
}

/************
 * Solution *
 ************/

struct Solution {          // A solution is a set of vertices
    unsigned int capacity; // The number of vertices
    bool *included;        // included[u] iff u is in the solution
};

/**
 * Returns the number of elements in the solution.
 *
 * @param solution  The solution
 * @return          Its size
 */
unsigned int solution_size(const struct Solution *solution) {
    unsigned int size = 0;
    for (unsigned int u = 0; u < solution->capacity; ++u) {
        size += solution->included[u] ? 1 : 0;
    }
    return size;
}

/**
 * Prints the solution to `stdout`.
 *
 * @param solution  The solution to print
 */
void print_solution(const struct Solution *solution) {
    printf("[ ");
    for (unsigned int u = 0; u < solution->capacity; ++u) {
        if (solution->included[u]) printf("%d ", u);
    }
    printf("]");
}

/**
 * Checks if a solution is a vertex cover of a graph.
 *
 * @param graph     The graph
 * @param solution  The solution
 * @return          True iff the solution is a cover of the graph
 */
bool is_vertex_cover(const struct Graph *graph,
                     const struct Solution *solution) {
    for (unsigned int u = 0; u < graph->num_vertices; ++u) {
        for (unsigned int v = u + 1; v < graph->num_vertices; ++v) {
            if (graph->edges[u][v] &&
                !solution->included[u] &&
                !solution->included[v]) {
                return false;
            }
        }
    }
    return true;
}

/**********
 * Search *
 **********/

struct Search {                         // A naive search
    const struct Graph *graph;          // The graph to search
    struct Solution *solution;          // The current solution in the search
    unsigned int optimal_size;          // The size of an optimal solution
    unsigned int num_optimal_solutions; // The number of optimal solutions
    bool verbose;                       // If true, displays trace details
};

/**
 * Recursively count the number of optimal solutions.
 *
 * @param search  The search
 * @param depth   The current depth
 */
void compute_num_optimal_solutions(struct Search *search,
                                   unsigned int depth) {
    if (depth < search->graph->num_vertices) {
        search->solution->included[depth] = true;
        compute_num_optimal_solutions(search, depth + 1);
        search->solution->included[depth] = false;
        compute_num_optimal_solutions(search, depth + 1);
    } else if (is_vertex_cover(search->graph, search->solution)) {
        unsigned int size = solution_size(search->solution);
        if (size < search->optimal_size) {
            search->optimal_size = size;
            search->num_optimal_solutions = 1;
            if (search->verbose) {
                printf("Found a better solution: ");
                print_solution(search->solution);
                printf("\n");
            }
        } else if (size == search->optimal_size) {
            ++search->num_optimal_solutions;
            if (search->verbose) {
                printf("Found another currently optimal solution: ");
                print_solution(search->solution);
                printf("\n");
            }
        }
    }
}

/**
 * Returns the number of optimal solutions.
 *
 * @param graph    The graph to search
 * @param verbose  If true, prints trace to `stdout`
 * @return         The number of optimal solutions
 */
unsigned int num_optimal_solutions(const struct Graph *graph,
                                   bool verbose) {
    struct Search search = {
        .graph                 = graph,
        .solution              = malloc(sizeof(struct Solution)),
        .optimal_size          = graph->num_vertices + 1,
        .num_optimal_solutions = 0,
        .verbose               = verbose
    };
    search.solution->capacity = graph->num_vertices;
    search.solution->included = calloc(graph->num_vertices, sizeof(bool));
    compute_num_optimal_solutions(&search, 0);
    free(search.solution->included);
    free(search.solution);
    return search.num_optimal_solutions;
}

/********
 * Main *
 ********/

int main(int argc, char *argv[]) {
    bool verbose = false, quiet = false;

    if (argc == 2) {
        if      (strcmp(argv[1], "verbose") == 0) verbose = true;
        else if (strcmp(argv[1], "quiet") == 0)   quiet = true;
    }

    if (verbose) {
        printf("Loading the graph...\n");
    }

    struct Graph graph;
    load_graph(&graph);
    if (verbose) print_graph(&graph);
    unsigned int n = num_optimal_solutions(&graph, verbose);

    if (verbose) {
        printf("Naive algorithm...\n");
        printf("Number of optimal solutions: ");
    }
    if (!quiet) printf("%d\n", n);

    return 0;
}
