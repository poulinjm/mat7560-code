# Implémentation - MAT5071 Optimisation combinatoire

Ce dépôt contient le code source d'exemples présentés dans le cadre du cours
MAT5071 Optimisation combinatoire, à l'Université du Québec à Montréal.

* [`vertex-cover`](./vertex-cover)
